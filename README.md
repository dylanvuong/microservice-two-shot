# Wardrobify

Team:

* Person 1 - Shoes Microserve
* Person 2 - None

## Design

## Shoes microservice

The ShoeForm is brought up upon clicking "Create Shoes". The user is able to create a new pair of shoes by filling out all the text fields. There is a dropdown menu for selecting bin. Created shoes will reflect in "Shoes List"

The ListShoes is brought up upon clicking "Shoes List". The user is able to view all the created shoes listed with names, pictures, and bin number. The user is able to delete the item from the page through a "delete" button.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
