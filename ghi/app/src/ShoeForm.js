import React, { useEffect, useState } from 'react';

function ShoeForm(props) {
  const [name, setName] = useState('');
  const [color, setColor] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [picture, setPicture] = useState('');
  const [bin, setBin] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {
      model_name: name,
      color: color,
      manufacturer: manufacturer,
      picture_url: picture,
      bin: bin
    };

    const shoeUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(shoeUrl, fetchConfig)
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe);
      setName('');
      setColor('');
      setManufacturer('');
      setPicture('');
      setBin('');
    }
  }

  function handleChange(event, callback) {
    const { target } = event;
    const { value } = target;
    callback(value);
  }

  const handleBinChange = (event) => {
    const value = event.target.value;
    const selectedBin = bins.find(bin => bin.href === value);
    if (selectedBin) {
      setBin(selectedBin.id);
    } else {
      setBin('');
    }
  }

  const [bins, setBins] = useState([]);
  const fetchData = async () => {
    const url = 'http://localhost:8100/api/bins/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className='row'>
      <div className='offset-3 col-6'>
        <div className='shadow p-4 mt-4'>
          <h1 className="text-center">Let's create a new pair of shoes!</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                onChange={(event) => handleChange(event, setName)}
                className="form-control"
                value={name}
                placeholder='name your shoe'
                required
                type="text"
                name="name"
                id="name"
              />
              <label htmlFor="name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(event) => handleChange(event, setColor)}
                className="form-control"
                value={color}
                placeholder='color'
                required
                type="text"
                name="color"
                id="color"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(event) => handleChange(event, setManufacturer)}
                className="form-control"
                value={manufacturer}
                placeholder='manufacturer'
                required
                type="text"
                name="manufacturer"
                id="manufacturer"
              />
              <label htmlFor="manufacturer">Brand Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(event) => handleChange(event, setPicture)}
                className="form-control"
                value={picture}
                placeholder='picture url'
                required
                type="text"
                name="picture_url"
                id="picture_url"
              />
              <label htmlFor="picture_url">Picture url</label>
            </div>
            <div className="mb-3">
              <label htmlFor="bin" className="form-label">Bin</label>
              <select
                onChange={handleBinChange}
                required
                name="bin"
                id="bin"
                className="form-select"
              >
                <option value="">Choose a Bin</option>
                {bins.map(bin => (
                  <option key={bin.href} value={bin.href}>
                    {bin.bin_number}
                  </option>
                ))}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
