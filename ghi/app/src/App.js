import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Shoes from './ListShoes';
import ShoeForm from './ShoeForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index path="/" element={<MainPage />} />
          <Route path="shoes/" element={<Shoes shoes={props.shoes}/>} />
          <Route path="shoes/new/" element={<ShoeForm/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
